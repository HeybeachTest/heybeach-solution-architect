# Overview
Imagine you have to implement an api for a new venture called heybeach. heybeach will revolutionize the vacation industry by providing excellent quality beach pictures to its users.

Management does not yet know if the business model is sound. Therefore, it was decided to quickly develop an MVP version of the business in order to test the assumptions with actual customers.

Your task is to build a working version of this MVP.

# MVP Description
The main idea of the product is that every user can both upload beach photos and view uploaded images. In your MVP version, the api should include:

- new user signup
- user authentication
- user ability to upload new images of beaches
- user ability to retrieve images

After you have finished your task, you will hand off your work to a frontend engineer who will build a website based on your api.

*Optional:* If time allows, provide a simple frontend solution to demonstrate the use case.

As management wants to have the site online as quickly as possible, it is important to use existing software to build a simple, functional solution with minimal effort. It is encouraged to use tools / libraries / programming language / saas solutions which will provide the basic functionality in a timely fashion. You are allowed to take shortcuts!

# What we are looking for
- a running solution which can be accessed online
- code / instructions which are required to run your solution available in a git repository
- reasons why solutions were used or not used
- documentation / examples on how to work with the api, so a frontend engineer could easily take over

Be able to explain your process clearly! Please write down what was most important for you in this project, why you focused on this, how you spent your time, why you made certain decisions and how you implemented them.

# Final Points
Treat this as you would an actual project. The time limit, starting now, is **7 days**. You can and should make decisions on your own, as long as you are able to justify them. If you are not certain about any part of the challenge, please feel free to ask for clarification. We are primarily interested in your process & are happy to address questions that may arise. 


